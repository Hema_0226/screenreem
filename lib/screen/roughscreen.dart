import 'package:flutter/material.dart';

// ignore: must_be_immutable
class RoughScreen extends StatefulWidget {
  String iconImage;
  String transName;
  String transSub;
  String transTrail1;
  String transResult;
  String transTrail2;
  Color color;
  RoughScreen(this.iconImage, this.transName, this.transSub, this.transTrail1,
      this.transResult, this.transTrail2,
      {this.color});

  @override
  _RoughScreenState createState() => _RoughScreenState();
}

class _RoughScreenState extends State<RoughScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 31, right: 20),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey[300]),
          borderRadius: BorderRadius.circular(15),
        ),
        child: Row(children: [
          Container(
              height: 21, width: 21, child: Image.asset(widget.iconImage)),
          Column(
            children: [
              Text(widget.transName),
              Text(widget.transSub),
            ],
          ),
          Text(widget.transTrail1),
          Column(
            children: [
              Text(widget.transTrail2),
              Text(
                widget.transResult,
                style: TextStyle(
                  color: widget.color,
                ),
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
