import 'package:flutter/material.dart';

import 'home_screen.dart';

class BottomTabBars extends StatefulWidget {
  @override
  _BottomTabBarsState createState() => _BottomTabBarsState();
}

class _BottomTabBarsState extends State<BottomTabBars> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    HomeScreen(),
    Container(
      color: Colors.white,
    ),
    Container(
      color: Colors.green,
    ),
    Container(
      color: Colors.pink,
    ),
  ];
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          onTap: onTabTapped,
          items: [
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage("images/Home.png")),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage("images/Swap.png")),
              // size: 100,

              label: 'History',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/Frame.png"),
              ),
              label: 'Card settings',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("images/Profile.png"),
              ),
              label: 'Profile',
            ),
          ],
          type: BottomNavigationBarType.shifting,
          selectedItemColor: Colors.pink,
          unselectedItemColor: Colors.grey,
          iconSize: 30,
          elevation: 5),
    );
  }
}
