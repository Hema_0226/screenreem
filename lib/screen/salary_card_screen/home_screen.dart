import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:reem_app/screen/salary_card_screen/bottom_tabbar_screen.dart';
import 'package:reem_app/utils/color_resource.dart';
import 'package:reem_app/utils/image_resource.dart';
import 'package:reem_app/utils/string_resource.dart';
import 'package:reem_app/widgets/salary_page_widget.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(children: [
          Container(
            margin: const EdgeInsets.only(left: 157),
            width: double.infinity,
            // height: 318,
            child: Image.asset(
              ImageResource.bg,
              color: Colors.grey,
              fit: BoxFit.fill,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 25, left: 30),
            child: Row(children: [
              Text(
                'Good morning, Rio!',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 143),
                child: Image.asset("images/Notification.png"),
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 23.5),
                  child: Image.asset("images/chat.png")),
            ]),
          ),
          Expanded(
            child: SingleChildScrollView(
                child: Column(children: [
              _buildSalaryCard(),
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 30, right: 30),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.circular(15)),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      children: [
                        Column(children: [
                          Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Text(
                                StringResource.salaryBalance,
                                style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w600,
                                ),
                              )),
                          Wrap(children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 5, left: 10),
                              child: Text(
                                StringResource.aed1400,
                                style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 7),
                              child: Text(
                                StringResource.aedn1400,
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ]),
                        ]),
                        Divider(
                          thickness: 5,
                          color: Colors.black,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 35, top: 15),
                          child: Column(
                            children: [
                              Text(
                                StringResource.creditLimit,
                                style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Wrap(children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 5),
                                  child: Text(
                                    StringResource.aed2400,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 7, left: 3),
                                  child: Text(
                                    StringResource.aedn1400,
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ]),
                              Divider(
                                color: Colors.red,
                                thickness: 4,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 35,
                          ),
                          child: Column(
                            children: [
                              Text(
                                StringResource.dueaAmount,
                                style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Wrap(children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 5),
                                  child: Text(
                                    StringResource.aed1000,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 7, left: 3),
                                  child: Text(
                                    StringResource.aedn1400,
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                )
                              ]),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: ColorResource.color000000
                            .withOpacity(0.25), //color of shadow
                        spreadRadius: 0, //spread radius
                        blurRadius: 5, // blur radius
                        offset: Offset(5, 5), // changes position of shadow
                        //first paramerter of offset is left-right
                        //second parameter is top to down
                      ),
                      //you can set more BoxShadow() here
                    ],
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40),
                      bottomRight: Radius.circular(40),
                    )),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 18, left: 30, right: 30, bottom: 30),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BottomTabBars()));
                    },
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                        color: ColorResource.colorD71E60,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Center(
                        child: Text(
                          StringResource.payDueAmount,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30, right: 270),
                child: Text(
                  StringResource.features,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w700),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, left: 30, right: 30),
                child: Row(
                  children: [
                    Container(
                      height: 52,
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey[200],
                          ),
                          borderRadius: BorderRadius.circular(13)),
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Wrap(children: [
                          Container(
                              height: 40,
                              width: 40,
                              child: Padding(
                                padding: const EdgeInsets.only(top: 3, left: 5),
                                child: Image.asset(ImageResource.group3199),
                              )),
                          Padding(
                            padding: const EdgeInsets.only(top: 11, right: 20),
                            child: Text(StringResource.sendMoney),
                          ),
                        ]),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Container(
                        height: 52,
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.grey[200],
                            ),
                            borderRadius: BorderRadius.circular(13)),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Wrap(children: [
                            Container(
                                height: 40,
                                width: 40,
                                child: Padding(
                                  padding:
                                      const EdgeInsets.only(top: 3, left: 5),
                                  child: Image.asset(ImageResource.group6054),
                                )),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 11, right: 20),
                              child: Text(StringResource.billPay),
                            ),
                          ]),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              // Padding(
              //   padding: const EdgeInsets.only(top: 26),
              //   child: Row(
              //     children: [
              //       Padding(
              //         padding: const EdgeInsets.only(left: 30),
              //         child: Expanded(
              //           flex: 1,
              //           child: Container(
              //               height: 40,
              //               width: 40,
              //               child: Image.asset(ImageResource.group3199)),
              //         ),
              //       ),
              //       Text(StringResource.sendMoney),
              //       Padding(
              //         padding: const EdgeInsets.only(left: 20),
              //         child: Container(
              //             height: 40,
              //             width: 40,
              //             child: Image.asset(ImageResource.group6054)),
              //       ),
              //       Text(StringResource.billPay),
              //     ],
              //   ),
              // ),
              Padding(
                padding: const EdgeInsets.only(top: 30, right: 200),
                child: Text(
                  StringResource.offersFromReem,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w700),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 15, left: 30, right: 30),
                height: 200,
                child: ListView(scrollDirection: Axis.horizontal, children: [
                  Container(
                    height: 150,
                    child: Image.asset(
                      ImageResource.hand,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Container(
                      height: 150,
                      child: Image.asset(
                        ImageResource.hand,
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                ]),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Image.asset(ImageResource.load),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30, right: 180),
                child: Text(
                  StringResource.recentTrans,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w700),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 19),
                child: SalaryPageWidget(
                  ImageResource.group6068,
                  StringResource.starBucks,
                  StringResource.date,
                  StringResource.aedn1400,
                  StringResource.sent,
                  StringResource.trans99,
                  color: Colors.green,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 19),
                child: SalaryPageWidget(
                  ImageResource.group6068,
                  StringResource.dubai,
                  StringResource.date,
                  StringResource.aedn1400,
                  StringResource.processing,
                  StringResource.trans78,
                  color: Colors.yellow,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 19),
                child: SalaryPageWidget(
                  ImageResource.group6068,
                  StringResource.led40,
                  StringResource.date,
                  StringResource.aedn1400,
                  StringResource.failed,
                  StringResource.trans78,
                  color: Colors.red,
                ),
              ),
            ])),
          ),
        ]));
  }

  Widget _buildSalaryCard() {
    return Container(
      margin: EdgeInsets.only(left: 30, right: 30, top: 60),
      decoration: BoxDecoration(
          color: ColorResource.color7C1F60,
          borderRadius: BorderRadius.circular(30)),
      child: Stack(
        children: [
          Container(
            height: 200,
            width: 351,
            child: Image.asset(
              ImageResource.ellipseMask,
              fit: BoxFit.fill,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 29, left: 30),
            child: Text(
              StringResource.salaryCard,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 120, left: 33),
            child: Text(
              StringResource.balance,
              style: TextStyle(
                color: Colors.white,
                fontSize: 12,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 135, left: 20),
            child: Text(
              StringResource.balnceAed,
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
