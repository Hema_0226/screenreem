import 'package:flutter/cupertino.dart';

class ColorResource {
  static const Color color242424 = Color(0xff242424);
  static const Color colorFFFFFF = Color(0xffFFFFFF);
  static const Color color8A8A8A = Color(0xff8A8A8A);
  static const Color color06C835 = Color(0xff06C835);
  static const Color colorE8C000 = Color(0xffE8c000);
  static const Color colorFB3A3A = Color(0xffFB3A3A);
  static const Color color6E7CA3 = Color(0xff6E7CA3);
  static const Color color7C1F60 = Color(0xff7C1F60);
  static const Color colorF5F5F5 = Color(0xffF5F5F5);
  static const Color colorD71E60 = Color(0xffD71E60);
  static const Color color000000 = Color(0xff000000);
}
