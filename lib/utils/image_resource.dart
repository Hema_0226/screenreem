class ImageResource {
  static const String chat = "images/chat.png";
  static const String notification = "images/Notification.png";
  static const String ellipseMask = "images/Mask Group.png";
  static const String bg = "images/bg.png";
  static const String group3199 = "images/Group 3199.png";
  static const String group6054 = "images/Group 6054.png";
  static const String image9 = "images/image 9(1).png";
  static const String group6068 = "images/Group 6068.png";
  static const String home = "images/Home.png";
  static const String swap = "images/Swap.png";
  static const String frame = "images/Frame.png";
  static const String profile = "images/Profile.png";
  static const String hand = "images/Hand.png";
  static const String load = "images/Group 33621.png";
}
