class StringResource {
  static const String goodMorning = 'Good morning, Rio!';
  static const String salaryCard = 'Salary Card';
  static const String rioRaj = 'Rio Raj';
  static const String balance = 'Your Balance';
  static const String balnceAed = '2600 AED';
  static const String salaryBalance = 'Salary Balance';
  static const String creditLimit = 'Credit Limit';
  static const String dueaAmount = 'Due Amount';
  static const String aed1400 = '1400 ';
  static const String aedn1400 = 'AED';
  static const String aed1000 = '1000';
  static const String aed2400 = '2400';
  static const String payDueAmount = 'PAY DUE AMOUNT';
  static const String sendMoney = 'Send Money';
  static const String billPay = 'Bill Payments';
  static const String features = 'Features';
  static const String offersFromReem = 'Offers from REEM';
  static const String recentTrans = 'Recent Transactions';
  static const String starBucks = 'Star Bucks';
  static const String trans99 = '99.00';
  static const String dubai = 'Dubai Restraunt';
  static const String trans78 = '78.45';
  static const String sent = 'Sent';
  static const String processing = 'Process';
  static const String led40 = 'Led TV 40 Inc...';
  static const String failed = "Failed";
  static const String home = 'Home';
  static const String history = 'History';
  static const String cardSettings = 'Card Settings';

  static const String profile = 'Profile';
  static const String date = 'Dec 31, 2020 8:45 pm';
}
