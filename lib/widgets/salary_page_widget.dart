import 'package:flutter/material.dart';

// ignore: must_be_immutable
class SalaryPageWidget extends StatefulWidget {
  String iconImage;
  String transName;
  String transSub;
  String transTrail1;
  String transResult;
  String transTrail2;
  Color color;
  SalaryPageWidget(this.iconImage, this.transName, this.transSub,
      this.transTrail1, this.transResult, this.transTrail2,
      {this.color});
  @override
  _SalaryPageWidgetState createState() => _SalaryPageWidgetState();
}

class _SalaryPageWidgetState extends State<SalaryPageWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 31, right: 30),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey[300]),
          borderRadius: BorderRadius.circular(15),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 15),
          child: Row(children: [
            Container(
                height: 21, width: 21, child: Image.asset(widget.iconImage)),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 30),
                    child: Text(widget.transName),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Text(widget.transSub),
                  ),
                ],
              ),
            ),
            // Text(widget.transTrail1),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 50),
                  child: Text(widget.transTrail2),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 50),
                  child: Text(
                    widget.transResult,
                    style: TextStyle(
                      color: widget.color,
                    ),
                  ),
                ),
              ],
            ),
          ]),
        ),
      ),
    );
  }
}
